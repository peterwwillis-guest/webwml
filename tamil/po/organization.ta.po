# translation of organization.po to தமிழ்
# ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2007-11-05 15:36+0530\n"
"Last-Translator: ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>\n"
"Language-Team: தமிழ் <ubuntu-l10n-tam@lists.ubuntu.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Tamil\n"
"X-Poedit-Country: INDIA\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: KBabel 1.11.4\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "தற்போதைய"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "உறுப்பினர்"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:34
#, fuzzy
#| msgid "Release Managers"
msgid "Stable Release Manager"
msgstr "வெளியீட்டு மேலாளர்"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr ""

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "தலைவர்"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "உதவியாளர்"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "செயலர்"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "அலுவலர்கள்"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:87
msgid "Distribution"
msgstr "வழங்கல்"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:229
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:232
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "விளம்பரம்"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:300
msgid "Support and Infrastructure"
msgstr "ஆதரவும் கட்டமைப்பும்"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "அணித்தலைவர்"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "தொழில்நுட்பக் குழு"

#: ../../english/intro/organization.data:82
msgid "Secretary"
msgstr "செயலர்"

#: ../../english/intro/organization.data:90
msgid "Development Projects"
msgstr "உருவாக்கத் திட்டங்கள்"

#: ../../english/intro/organization.data:91
msgid "FTP Archives"
msgstr "கோபநெ பெட்டகம்"

#: ../../english/intro/organization.data:93
#, fuzzy
#| msgid "FTP Master"
msgid "FTP Masters"
msgstr "கோபநெ முதல்வர்"

#: ../../english/intro/organization.data:99
msgid "FTP Assistants"
msgstr "கோபநெ துணைவோர்"

#: ../../english/intro/organization.data:104
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:108
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:110
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:114
msgid "Individual Packages"
msgstr "தனிப்பட்ட பொதிகள்"

#: ../../english/intro/organization.data:115
msgid "Release Management"
msgstr "வெளியீடு நிர்வாகம்"

#: ../../english/intro/organization.data:117
#, fuzzy
#| msgid "Release Notes"
msgid "Release Team"
msgstr "வெளியீட்டுக் குறிப்புகள்"

#: ../../english/intro/organization.data:130
msgid "Quality Assurance"
msgstr "தர உத்தரவாதம்"

#: ../../english/intro/organization.data:131
msgid "Installation System Team"
msgstr "நிறுவலுக்கான குழுமம்"

#: ../../english/intro/organization.data:132
msgid "Release Notes"
msgstr "வெளியீட்டுக் குறிப்புகள்"

#: ../../english/intro/organization.data:134
msgid "CD Images"
msgstr "குறு வட்டு பிம்பங்கள்"

#: ../../english/intro/organization.data:136
msgid "Production"
msgstr "உருவாக்கம்"

#: ../../english/intro/organization.data:144
msgid "Testing"
msgstr "சோதனை"

#: ../../english/intro/organization.data:146
#, fuzzy
#| msgid "Support and Infrastructure"
msgid "Autobuilding infrastructure"
msgstr "ஆதரவும் கட்டமைப்பும்"

#: ../../english/intro/organization.data:148
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:156
#, fuzzy
#| msgid "Buildd Administration"
msgid "Buildd administration"
msgstr "சமை நிர்வாகம்"

#: ../../english/intro/organization.data:175
msgid "Documentation"
msgstr "ஆவணமாக்கம்"

#: ../../english/intro/organization.data:180
msgid "Work-Needing and Prospective Packages list"
msgstr "பணித்-தேவைப் படுபவை மற்றும் வாய்ப்புள்ள பொதிகளின் பட்டியல்"

#: ../../english/intro/organization.data:183
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:184
msgid "Ports"
msgstr "துறைகள்"

#: ../../english/intro/organization.data:219
msgid "Special Configurations"
msgstr "சிறப்பு வடிவமைப்புகள்"

#: ../../english/intro/organization.data:222
msgid "Laptops"
msgstr "மடிக்கணினிகள்"

#: ../../english/intro/organization.data:223
msgid "Firewalls"
msgstr "அரண்கள்"

#: ../../english/intro/organization.data:224
msgid "Embedded systems"
msgstr "உட்பொதிந்த சாதனங்கள்"

#: ../../english/intro/organization.data:237
msgid "Press Contact"
msgstr "பத்திரிக்கை தொடர்பு"

#: ../../english/intro/organization.data:239
msgid "Web Pages"
msgstr "இணையப் பக்கங்கள்"

#: ../../english/intro/organization.data:249
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:254
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:258
msgid "Debian Women Project"
msgstr "பெண்களுக்கான டெபியன் திட்டம்"

#: ../../english/intro/organization.data:266
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:272
msgid "Events"
msgstr "சம்பவங்கள்"

#: ../../english/intro/organization.data:278
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "தொழில்நுட்பக் குழு"

#: ../../english/intro/organization.data:285
msgid "Partner Program"
msgstr "பங்குதாரர் திட்டம்"

#: ../../english/intro/organization.data:290
msgid "Hardware Donations Coordination"
msgstr "வன்பொருள் தான ஒருங்கிணைப்பு"

#: ../../english/intro/organization.data:303
msgid "User support"
msgstr "பயனர் ஆதரவு"

#: ../../english/intro/organization.data:370
msgid "Bug Tracking System"
msgstr "வழுக் கண்காணிப்பு அமைப்பு"

#: ../../english/intro/organization.data:375
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "மடலாடற் குழு நிர்வாகம் மற்றும் மடலாடற் குழு பெட்டகம்"

#: ../../english/intro/organization.data:383
#, fuzzy
#| msgid "New Maintainers Front Desk"
msgid "New Members Front Desk"
msgstr "புதிய பராமமரிப்பாளர்கள் முன்னணி"

#: ../../english/intro/organization.data:389
msgid "Debian Account Managers"
msgstr "டெபியன் கணக்கு மேலாளர்கள்"

#: ../../english/intro/organization.data:393
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:394
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "துருப்பு பராமரிப்பாளர்கள் (பிஜிபி மற்றும் ஜிபிஜி)"

#: ../../english/intro/organization.data:397
msgid "Security Team"
msgstr "பாதுகாப்புக் குழு"

#: ../../english/intro/organization.data:409
msgid "Consultants Page"
msgstr "ஆலோசகர் பக்கம்"

#: ../../english/intro/organization.data:414
msgid "CD Vendors Page"
msgstr "வட்டு வழங்குவோர் பக்கம்"

#: ../../english/intro/organization.data:417
msgid "Policy"
msgstr "கொள்கை"

#: ../../english/intro/organization.data:422
msgid "System Administration"
msgstr "அமைப்பு நிர்வாகம்"

#: ../../english/intro/organization.data:423
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"ஏதெனுமொரு டெபியன் இயந்திரத்தில், கடவுச்சொல் உள்ளிட்ட கோளாறு ஏற்படின் பயன்படுத்தப் பட "
"வேண்டிய முகவரி இது. இல்லையெனில் தாங்கள் நிறுவிட பொதியொன்றுத் தேவைப் படுகிறது.   "

#: ../../english/intro/organization.data:432
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"டெபியன் இயந்திரங்களில் வன்பிரச்சனைகள் இருப்பின், <a href=\"https://db.debian.org/"
"machines.cgi\">டெபியன் இயந்திரங்கள்</a> பக்கத்தினை அணுகவும், பிரதி இயந்திரத்துக்கான "
"நிர்வாகத் தகவலை அது கொண்டிருக்கலாம்."

#: ../../english/intro/organization.data:433
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP உருவாக்குநர் அடைவு நிர்வாகி"

#: ../../english/intro/organization.data:434
msgid "Mirrors"
msgstr "பிம்பங்கள்"

#: ../../english/intro/organization.data:441
msgid "DNS Maintainer"
msgstr "DNS பராமரிப்பாளர்"

#: ../../english/intro/organization.data:442
msgid "Package Tracking System"
msgstr "பொதி கண்காணிப்பு அமைப்பு"

#: ../../english/intro/organization.data:444
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:450
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:453
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "அலியோத் நிர்வாகிகள்"

#: ../../english/intro/organization.data:457
msgid "Alioth administrators"
msgstr "அலியோத் நிர்வாகிகள்"

#: ../../english/intro/organization.data:470
msgid "Debian for children from 1 to 99"
msgstr "1 முதல் 99 வரைக் குழந்தைகளுக்கான டெபியன்"

#: ../../english/intro/organization.data:473
msgid "Debian for medical practice and research"
msgstr "மருத்துவப் பழகலுக்கும் ஆராய்ச்சிக்கும் டெபியன்"

#: ../../english/intro/organization.data:476
msgid "Debian for education"
msgstr "கல்விக்கான டெபியன்"

#: ../../english/intro/organization.data:481
msgid "Debian in legal offices"
msgstr "சட்ட அலுவலகங்களில் டெபியன்"

#: ../../english/intro/organization.data:485
msgid "Debian for people with disabilities"
msgstr "ஊனமுற்றோருக்கான டெபியன்"

#: ../../english/intro/organization.data:489
#, fuzzy
#| msgid "Debian for medical practice and research"
msgid "Debian for science and related research"
msgstr "மருத்துவப் பழகலுக்கும் ஆராய்ச்சிக்கும் டெபியன்"

#: ../../english/intro/organization.data:492
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "கல்விக்கான டெபியன்"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "பாதுகாப்புக் குழு"

#~ msgid "Security Audit Project"
#~ msgstr "பாதுகாப்பு தணிக்கைத் திட்டம்"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "இது முறையான டெபியன் திட்டமல்ல. ஆனால் அது ஒருங்கிணைக்கப் படுவதற்கான நோக்கம் "
#~ "அறிவிக்கப்பட்டுள்ளதுதப் பட்டுள்ளது."

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "டெபியன் பல்லூடக வழங்கல்"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "இலாப நோக்கற்ற நிறுவனங்களுக்கான டெபியன்"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "அகிலத்துக்குமான இயங்கு தளம்"

#~ msgid "Accountant"
#~ msgstr "கணக்காளர்"

#~ msgid "Key Signing Coordination"
#~ msgstr "துருப்பு ஒப்புமை ஒருங்கிணைப்பு"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "தனிப்பட்ட சமைப்புக்கான நிர்வாகியின் பெயரை <a href=\"http://www.buildd.net"
#~ "\">http://www.buildd.net</a> முகவரியில் காணலாம். கட்டமைப்பு ஒன்றையும் "
#~ "வழங்கலொன்றையும் தேர்வு செய்து கிடைக்கக் கூடிய சமைப்புகளையும் அவற்றின் நிர்வாகிகளையும் "
#~ "காணலாம்."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "குறிப்பிட்ட கட்டமைப்புக்கு ஏற்றாற் போல் சமைத்தலுக்கான நிர்வாகிகளை <genericemail "
#~ "arch@buildd.debian.org> முகவரியில் தொடர்புக் கொள்ளலாம். உதாரணம் <genericemail "
#~ "i386@buildd.debian.org>."

#~ msgid "Security Testing Team"
#~ msgstr "பாதுகாப்புச் சோதனைக் குழு"

#~ msgid "Handhelds"
#~ msgstr "கையடக்க"

#~ msgid "APT Team"
#~ msgstr "ஏபிடி குழு"

#~ msgid "Vendors"
#~ msgstr "வழங்குவோர்"

#~ msgid "Release Wizard"
#~ msgstr "வெளியீட்டு பலகை"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "``நிலையானவைக்கான'' வெளியீட்டு உதவியாளர்கள்"

#~ msgid "Release Assistants"
#~ msgstr "வெளியீட்டு உதவியாளர்கள்"

#~ msgid "Release Managers for ``stable''"
#~ msgstr "\"நிலையானவை\" க்கான வெளியீட்டு மேலாளர்"

#~ msgid "Custom Debian Distributions"
#~ msgstr "தனிப்பட்ட டெபியன் வழங்கல்கள்"

#~ msgid "Publicity"
#~ msgstr "விளம்பரம்"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "நிறுவலுக்கான குழுமம்"
